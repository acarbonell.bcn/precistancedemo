package com.company;

import com.mongodb.client.*;
import org.bson.Document;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Aggregates.set;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.*;
import static com.mongodb.client.model.Updates.combine;

public class DatabaseMongo implements Database{
    final static String uri = "mongodb://localhost";
    private MongoDatabase database;

    public void connect() throws CannotConnectException {
        try{
            MongoClient mongoClient = MongoClients.create(uri);
            database = mongoClient.getDatabase("sampledb");
        } catch (Exception e) {
            throw new CannotConnectException();
        }
    }

    public void insertMovie(String title) {
            database.getCollection("movies").insertOne(Movie.toDoc(new Movie(title)));
    }

    public void deleteMovie(String title) {
        database.getCollection("movies").deleteOne(Movie.toDoc(new Movie(title)));
    }


    @Override
    public void updateMovie(String title1, String title2) {
        database.getCollection("movies").updateOne(eq("title", title1), new Document("$set", new Document("title", title2)));
    }

    public Stream<Movie> queryAllMovie(){
        return StreamSupport.stream(
                database.getCollection("movies")
                        .find()
                        .projection(excludeId())
                        .spliterator(), false)
                .map(Movie::fromDoc);
    }
}
