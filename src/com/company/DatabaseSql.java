package com.company;

import com.mysql.cj.QueryResult;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class DatabaseSql implements Database {
    final static String uri = "jdbc:mysql://localhost/mydatabase?user=myuser&password=mypass";
    private Connection conn;

    public void insertMovie(String title) throws SQLException {
        PreparedStatement statement = conn.prepareStatement("INSERT INTO movies(title) VALUES(?)");
            statement.setString(1, title);
            statement.executeUpdate();
    }

    public Stream<Movie> queryAllMovie() throws SQLException {
        List<Movie> m = new ArrayList<>();
            ResultSet resultSet = conn.createStatement().executeQuery("SELECT * FROM movies");
            while (resultSet.next()) {
                m.add(new Movie(resultSet.getString("title")));
                System.out.println(resultSet.getString("title"));
            }
        //return m.stream();
        return null;

    }



    @Override
    public void connect() throws CannotConnectException {
        try {
            conn = DriverManager.getConnection(uri);
        } catch (Exception e) {
            throw new CannotConnectException();
        }
    }

    public void deleteMovie(String title){
        try(Connection conn = DriverManager.getConnection(uri)){
            PreparedStatement statement = conn.prepareStatement("DELETE FROM movies WHERE title = ?");
            statement.setString(1, title);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateMovie(String title1, String title2) {
        try(Connection conn = DriverManager.getConnection(uri)){
            PreparedStatement statement = conn.prepareStatement("UPDATE movies SET title = ? WHERE title = ?");
            statement.setString(1, title2);
            statement.setString(2, title1);
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }




}
