package com.company;

import java.sql.SQLException;
import java.util.stream.Stream;

public interface Database {
    void insertMovie(String title) throws SQLException;
    void deleteMovie(String title);
    void updateMovie(String title1, String title2);
    Stream<Movie> queryAllMovie() throws SQLException;
    void connect() throws CannotConnectException;
}
