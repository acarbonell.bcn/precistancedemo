package com.company;

import org.bson.Document;

public class Movie {
    String title;

    public Movie(String title) {
        this.title = title;
    }


    public static Movie fromDoc(Document doc){
        return new Movie(doc.getString("title"));
    }

    public static Document toDoc(Movie m){
        return new Document().append("title", m.title);
    }

}
