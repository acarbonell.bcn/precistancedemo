package com.company;

import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.List;
import java.util.Scanner;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;

public class Main {
    static Scanner sc = new Scanner(System.in);
    static Database db = null;
    public static void main(String[] args) throws SQLException {

        initDb();




        System.out.println("Quevols fer insert(i), update(u), delete(d), read(r) o quit(q)");
        String sel = sc.nextLine();
        while(sel != "q"){
            if ("i".equals(sel)){
                System.out.println("Title:");
                String title = sc.nextLine();
                //db.insertMovie(title);
            } else if ("d".equals(sel)){
                System.out.println("Title:");
                String title = sc.nextLine();
                db.deleteMovie(title);
            } else if ("u".equals(sel)){
                System.out.println("Old title:");
                String title1 = sc.nextLine();
                System.out.println("New title:");
                String title2 = sc.nextLine();
                db.updateMovie(title1, title2);
            } else if ("r".equals(sel)){
                db.queryAllMovie().forEach(movie -> {
                    System.out.println(movie.title);
                });
            } else {
                initDb();
            }

            System.out.println("Quevols fer insert(i), delete(d), read(r) o quit(q)");
            sel = sc.nextLine();
        }
    }

    private static void initDb(){
        System.out.println("Base de dades: mongo/mysql ?");
        String dbtype = sc.nextLine();
        if ("mysql".equals(dbtype)){
            db = new DatabaseSql();
        } else {
            db = new DatabaseMongo();
        }

        try{
            db.connect();
        } catch (CannotConnectException e) {
            System.out.println("Cannot connect to database");
            return;
        }
    }

}
